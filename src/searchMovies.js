import React, {useState} from "react"

    function SearchMovies() {

        const [query, setQuery] = useState('');
        const [movies, setMovies] = useState([]);

        const searchMovies = async (e) => {
            e.preventDefault();
            console.log('submitting')

            const url = `https://api.themoviedb.org/3/search/movie?api_key=e9d061d7a229fbdebfb395f3b4a073d8&language=en-US&query=${query}&page=1&include_adult=false`;

            try {
                const res = await fetch(url);

                const data = await res.json();

                //add movies data to state
                setMovies(data.results);

                console.log(data);
            } catch(err) {
                console.error(err);
            }   
        }

        return(
            <>
                <form className="form" onSubmit={searchMovies}>
                    <label htmlFor="query" className="label">Movie Name</label>
                    <input 
                        type="text" 
                        className="input" 
                        name="query"
                        placeholder="i.e. Jurassic Park"
                        value={query}
                        onChange={(e) => setQuery(e.target.value)}
                    />
                    <button className="button" type="submit">Search</button>

                </form>

                <div className="card-list">
                    {/** filter movies first and only keep items with a poster, then map through those*/}
                    {movies.filter(movie => movie.poster_path).map(movie => (
                        <div className="card" key={movie.id}>
                            
                            <img 
                                src={`https://image.tmdb.org/t/p/w185_and_h278_bestv2/${movie.poster_path}`} 
                                alt={movie.title + 'poster'} 
                                className="card--image"
                            />
                            <div className="card--content">
                                <h3 className="card--title">{movie.title}</h3>
                                <p><small>RELEASE DATE: {movie.release_date}</small></p>
                                <p><small>RATING: {movie.vote_average}</small></p>
                                <p className="card--description">{movie.overview}</p>
                            </div>

                        </div>
                    ))}
                </div>

            </>
        )
    }

export default SearchMovies